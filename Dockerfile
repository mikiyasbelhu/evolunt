# Stage 0, based on Node.js and Nginx, to build and compile Angular
FROM disyam/docker-nginx-node:8
WORKDIR /app
COPY package.json /app/
RUN npm install
COPY ./ /app/
ARG env=prod
RUN npm run build -- --prod --environment $env
RUN cp -rf /app/dist/. /usr/share/nginx/html/
RUN cp -rf ./nginx-custom.conf /etc/nginx/conf.d/default.conf
EXPOSE 4500

