import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/auth/index';
import { ProfileService } from '../../services/auth/profile.service';

declare var $: any;

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit{

  public profile;
  public name;
  public role;
  public photo;
  private loggedIn: boolean;
  private token;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    public profileService: ProfileService
  ) { }

  ngOnInit(): void {    

  this.name = this.profileService.getName();
  this.photo = this.profileService.getPhoto();
  this.role = (this.authService.getRole());
  this.role = (this.role).charAt(0);
  this.token = this.authService.getHeader();

  $('.ui.dropdown')
  .dropdown();

  $('.ui.search')
    .search({
      type          : 'category',
      minCharacters : 3,
      apiSettings   : {
        onResponse: function(evoluntResponse) {
          var
            response = {
              results : {}
            }
          ;
          // translate GitHub API response to work with search
          if((evoluntResponse.volunteer)){
            $.each(evoluntResponse.volunteer, function(index, item) {
              var
                category   = 'Volunteer',
                maxResults = 8
              ;
              if(index >= maxResults) {
                return false;
              }

              // create new category
              if(response.results[category] === undefined) {
                response.results[category] = {
                  name    : category,
                  results : []
                };
              }

              // add result to category
              response.results[category].results.push({
                title       : item.first_name + ' ' + item.last_name,
                description : item._id,
                url         : '/v/' + item._id + '/home'
              });
            });
          }

          if((evoluntResponse.charities)){
          $.each(evoluntResponse.charities, function(index, item) {
            var
            category   = 'Charity',
              maxResults = 8
            ;
            if(index >= maxResults) {
              return false;
            }
            // create new category
            if(response.results[category] === undefined) {
              response.results[category] = {
                name    : category,
                results : []
              };
            }

            // add result to category
            response.results[category].results.push({
              title       : item.name,
              description : item.description,
              url         : '/v/c/' + item._id + '/home'
            });
          });}

          $.each(evoluntResponse.event, function(index, item) {
            var
            category   = 'Event',
              maxResults = 8
            ;
            if(index >= maxResults) {
              return false;
            }
          
            // create new category
            if(response.results[category] === undefined) {
              response.results[category] = {
                name    : category,
                results : []
              };
            }

            response.results[category].results.push({
              title       : item.name,
              description : item.description,
              url         : item.html_url
            });

            // add result to category
            response.results[category].results.push({
              title       : item.name,
              description : item.description,
              url         : '/c/' + item._id + '/home'
            });
          });

          $.each(evoluntResponse.fund, function(index, item) {
            var
              category   = 'Fund',
              maxResults = 8
            ;
            if(index >= maxResults) {
              return false;
            }
            // create new category
            if(response.results[category] === undefined) {
              response.results[category] = {
                name    : category,
                results : []
              };
            }
            // add result to category
            response.results[category].results.push({
              title       : item.name,
              description : item.description,
              url         : '/c/' + item._id + '/home'
            });
          });

          return response;
        },
        url: 'http://volunteer.evolunt.addislaunch.com/search/{query}',
        headers: this.token
      }
    });

  }

  public logout() {
    this.authService.logout();
    // this.router.navigateByUrl('/');
  }

}
