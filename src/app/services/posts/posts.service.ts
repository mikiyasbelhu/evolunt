import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class PostsService extends BaseService implements HttpServiceInterface {

  private post_url = 'http://feed.evolunt.addislaunch.com/feeds';

  constructor(http: Http,authService:AuthenticationService) {
      super(http, 'http://feed.evolunt.addislaunch.com',authService);
  }
  
  public volunteerPost(id,data) {
    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
    this.options = new RequestOptions({headers: headers});
    return this.http.post(this.post_url + `/volunteer/${id}/postFeed`, data, this.options);
  }

  public charityPost(id,data) {
    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
    this.options = new RequestOptions({headers: headers});
    return this.http.post(this.post_url + `/charity/${id}/postFeed`, data, this.options);
  }


  public getFeed(id) {
    return this.http.get(this.post_url + `/${id}/getTimelineVolunteer`, this.options).retry(5);
  }

  public getCharityFeed(id) {
    return this.http.get(this.post_url + `/${id}/getTimelineCharity`, this.options).retry(5);
  }

  public getUserFeed(id) {
    return this.http.get(this.post_url + `/${id}/getUserFeed`, this.options).retry(5);
  }
  
  public like(postId,data){
    return this.http.post(this.post_url + `/${postId}/like`,data ,this.options).retry(5);
  } 

  public unlike(postId,data){
    return this.http.post(this.post_url + `/${postId}/unlike`,data ,this.options).retry(5);
  } 

  public comment(postId,data){
    return this.http.post(this.post_url + `/${postId}/comment`,data ,this.options).retry(5);
  } 

}
