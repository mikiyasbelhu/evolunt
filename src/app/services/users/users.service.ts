import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class UsersService extends BaseService implements HttpServiceInterface {
  constructor(http: Http,authService: AuthenticationService) {
    super(http, 'users',authService);
}
}
