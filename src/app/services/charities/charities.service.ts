import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class CharitiesService extends BaseService implements HttpServiceInterface {

  private charity_url = 'http://charity.evolunt.addislaunch.com';

  constructor(http: Http,authService: AuthenticationService) {
    super(http, 'http://charity.evolunt.addislaunch.com/charities/paginate',authService);
}

public get(id) {
  return this.http.get(this.charity_url + `/charities/${id}`, this.options).retry(5);
}

public update(id, data) {
  return this.http.put(this.charity_url + `/charities/${id}`, data, this.options).retry(5);
}

public follow(id, data) {
  return this.http.put(this.charity_url + `/charities/${id}/follow`, data, this.options).retry(5);
}

public unfollow(id, data) {
  return this.http.put(this.charity_url + `/charities/${id}/unfollow`, data, this.options).retry(5);
}

public following(id) {
  return this.http.get(this.charity_url + `/charities/${id}/getFollowing`, this.options).retry(5);
}

public upload(id, data) {
  const headers = new Headers();
  headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
  
  // headers.append('Content-Type', 'application/x-www-form-urlencoded');

  this.options = new RequestOptions({headers: headers});
  return this.http.put(this.charity_url + `/charities/${id}/pic`, data, this.options);
}

}
