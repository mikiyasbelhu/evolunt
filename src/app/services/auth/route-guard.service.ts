import { Injectable }     from '@angular/core';
import { CanActivate, Router }    from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class RouteGuard implements CanActivate {

  private role;

  constructor(private router: Router,
    private _authService: AuthenticationService,){
    
  }

  canActivate() {

    this.role = (this._authService.getRole());
    if(this.role){
      this.role = (this.role).charAt(0);
    }
    else {
      this.role = 'v';
      // this._authService.logout();
    }

    this.router.navigateByUrl('/'+this.role+'/home');

    return false;
  }
}
