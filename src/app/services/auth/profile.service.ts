import { Injectable, OnInit } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class ProfileService{
  
  public profile: string;

  constructor(public authService: AuthenticationService) {     this.authService
    .getProfile()
    .subscribe((user) =>{ 
      this.profile =  JSON.parse(user); 
    }
  );
  }

  public getName(){

  if(this.profile['first_name']){
    return this.profile['first_name'] +" " + this.profile['last_name'];
  }
  else{
    return this.profile['name'];
  }
  }

  public getPhoto(){

    if(this.profile['picture']){
      return this.profile['picture'];
    }
    else{
      return '/assets/images/sample.png';
    }
  }

  public getId(){
    
    if(this.profile['_id']){
        return this.profile['_id'];
    }
  }

  public getRole(){
    
    if(this.profile['role']){
        return this.profile['role'];
    }
  }

}
