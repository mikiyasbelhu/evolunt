import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from 'ngx-auth';
import 'rxjs/add/operator/switchMap';

import { environment } from './../../../environments/environment';

import { TokenStorage } from './token-storage.service';

interface AccessData {
  token: string;
  // refreshToken: string;
}

interface Profile {
  user: object;
}

@Injectable()
export class AuthenticationService implements AuthService {

  constructor(
    private http: Http,
    private tokenStorage: TokenStorage
  ) {}

  public isAuthorized(): Observable < boolean > {
    return this.tokenStorage
      .getAccessToken()
      .map(token => !!token);
  }

  public getAccessToken(): Observable < string > {
    return this.tokenStorage.getAccessToken()['value'];
  }

  public getRole(): Observable < string > {
    return this.tokenStorage.getRole()['value'];
  }

  public getProfile(): Observable < string > {
    return this.tokenStorage.getProfile();
  }

  public refreshToken(): Observable < AccessData > {
    return this.tokenStorage
      .getRefreshToken()
      .switchMap((refreshToken: string) => {
        return this.http.post(`http://localhost:3000/refresh`, { refreshToken });
      })
      .do(this.saveAccessData.bind(this))
      .catch((err) => {
        this.logout();

        return Observable.throw(err);
      });
  }

  public refreshShouldHappen(response: HttpErrorResponse): boolean {
    return response.status === 401
  }

  public verifyTokenRequest(url: string): boolean {
    return url.endsWith('/refresh');
  }

  public login(data): Observable<any> {
    return this.http.post(`http://auth.evolunt.addislaunch.com/auth/login`,data);

  }

  public signup(data): Observable<any> {
    return this.http.post(`http://auth.evolunt.addislaunch.com/auth/signup`, data);
  }

  public logout(): void {
    this.tokenStorage.clear();
    location.reload(true);
    
  }

  public saveAccessData(token) {
    this.tokenStorage
      .setAccessToken(token);
      // .setRefreshToken(refreshToken);
  }

  public saveRole(role) {
    this.tokenStorage
      .setRole(role);
  }

  public saveProfile(user) {
    this.tokenStorage
      .setProfile(user);
  }

  public getHeader() {
    return {'Authorization':'Bearer '+ this.getAccessToken()};
  }

}
