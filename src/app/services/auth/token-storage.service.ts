import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class TokenStorage {

  /**
   * Get access token
   * @returns {Observable<string>}
   */
  public getAccessToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('token');
    return Observable.of(token);
  }

    /**
   * Get profile
   * @returns {Observable<string>}
   */
  public getProfile(): Observable<string> {
    const user: string = <string>localStorage.getItem('profile');
    return Observable.of(user);
  }


   /**
   * Get refresh token
   * @returns {Observable<string>}
   */
  public getRole(): Observable<string> {
    const token: string = <string>localStorage.getItem('role');
    return Observable.of(token);
  } 

  /**
   * Get refresh token
   * @returns {Observable<string>}
   */
  public getRefreshToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('refreshToken');
    return Observable.of(token);
  }

  /**
   * Set access token
   * @returns {TokenStorage}
   */
  public setAccessToken(token: string): TokenStorage {
    localStorage.setItem('token', token);

    return this;
  }

  /**
   * Set profile
   * @returns {TokenStorage}
   */
  public setProfile(user: object): TokenStorage {
    localStorage.setItem('profile', JSON.stringify(user));

    return this;
  }


   /**
   * Set refresh token
   * @returns {TokenStorage}
   */
  public setRefreshToken(token: string): TokenStorage {
    localStorage.setItem('refreshToken', token);

    return this;
  }

   /**
   * Set role
   * @returns {TokenStorage}
   */
  public setRole(role: string): TokenStorage {
    localStorage.setItem('role', role);

    return this;
  }

   /**
   * Remove tokens
   */
  public clear() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('profile');
    localStorage.removeItem('AccessToken');
  }
}
