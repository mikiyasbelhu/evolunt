import { HttpServiceInterface } from './HttpServiceInterface';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { OnInit, Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { environment } from './../../environments/environment';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/retry'
import { AuthenticationService } from './auth/authentication.service';

export class BaseService implements HttpServiceInterface, OnInit {

  private model: string;
  public headers;
  public options;
  // public authService :AuthenticationService;

  private base_url;

  constructor(public http: Http, modelX: string,public authService:AuthenticationService) {
    this.model = modelX;
    this.base_url = this.model;

    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
    headers.append('Content-Type', 'application/json');

    this.options = new RequestOptions({headers: headers});
  }

  ngOnInit(): void {

  }

  public getAll() {
    return this.http.get(`${this.base_url}`,this.options).retry(5);
  }

  public get(id) {
    return this.http.get(`${this.base_url}/${id}`, this.options).retry(5);
  }

  public create(data) {
     return this.http.post(`${this.base_url}/`, data, this.options).retry(5);
  }

  public update(id, data) {
     return this.http.put(`${this.base_url}/${id}`, data, this.options).retry(5);
  }

  public delete(id) {
    return this.http.delete(`${this.base_url}/${id}`, this.options).retry(5);
  }
  
}
