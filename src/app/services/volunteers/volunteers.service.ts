import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class VolunteersService extends BaseService implements HttpServiceInterface {

  private volunteer_url = 'http://volunteer.evolunt.addislaunch.com';

  constructor(http: Http, public authService: AuthenticationService) {
    super(http,'http://volunteer.evolunt.addislaunch.com/volunteers/paginate',authService);
}

public get(id) {
  return this.http.get(this.volunteer_url + `/volunteers/${id}`, this.options).retry(5);
}

public update(id, data) {
  return this.http.put(this.volunteer_url + `/volunteers/${id}`, data, this.options).retry(5);
}

public follow(id, data) {
  return this.http.put(this.volunteer_url + `/volunteers/${id}/follow`, data, this.options).retry(5);
}

public unfollow(id, data) {
  return this.http.put(this.volunteer_url + `/volunteers/${id}/unfollow`, data, this.options).retry(5);
}

public following(id) {
  return this.http.get(this.volunteer_url + `/volunteers/${id}/following`, this.options).retry(5);
}

public attend(id, data) {
  return this.http.post(this.volunteer_url + `/volunteers/${id}/attend`, data, this.options).retry(5);
}

public interested(id, data) {
  return this.http.post(this.volunteer_url + `/volunteers/${id}/interested`, data, this.options).retry(5);
}

public contribute(id, data) {
  return this.http.post(this.volunteer_url + `/volunteers/${id}/contribute`, data, this.options).retry(5);
}

public upload(id, data) {
  const headers = new Headers();
  headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
  
  // headers.append('Content-Type', 'application/x-www-form-urlencoded');

  this.options = new RequestOptions({headers: headers});
  return this.http.put(this.volunteer_url + `/volunteers/pic/${id}`, data, this.options);
}
}
