import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class FundraiserService extends BaseService implements HttpServiceInterface {

  constructor(http: Http,authService:AuthenticationService) {
      super(http, 'http://event-fund.evolunt.addislaunch.com/funds/',authService);
  }

  public create(data) {
    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
    this.options = new RequestOptions({headers: headers});
    return this.http.post(`http://event-fund.evolunt.addislaunch.com/funds/`, data, this.options).retry(5);
 }
}
