import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable()
export class EventsService extends BaseService implements HttpServiceInterface {

  private event_fund_url = 'http://event-fund.evolunt.addislaunch.com';

  constructor(http: Http,authService:AuthenticationService) {
      super(http, 'http://event-fund.evolunt.addislaunch.com/events/',authService);
  }

  public upload(id, data) {
    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());

    this.options = new RequestOptions({headers: headers});
    return this.http.put(this.event_fund_url + `/events/${id}/pic`, data, this.options);
  }

  public create(data) {
    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
    this.options = new RequestOptions({headers: headers});
    return this.http.post(`http://event-fund.evolunt.addislaunch.com/events/`, data, this.options).retry(5);
 }


}
