import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthenticationService } from '../auth/authentication.service';


@Injectable()
export class ChatService extends BaseService implements HttpServiceInterface{

  constructor(http: Http,authService:AuthenticationService) {
    super(http, 'http://feed.evolunt.addislaunch.com',authService);
}
  public getConversation(data) {
      return this.http.post('http://178.62.247.200:5800/chat/getConvo?per_page=20', data, this.options)
  }

  public getInbox(data) {
    return this.http.post('http://178.62.247.200:5800/chat/inbox', data, this.options)
}

}
