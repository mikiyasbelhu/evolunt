import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';

import { HomeComponent } from './pages/volunteer/home/home.component';

const appRoutes: Routes = [
  {
    path: 'login',
    canActivate: [ PublicGuard ],
    loadChildren: './pages/common/login/login.module#LoginModule'
  },
  {
    path: 'signup',
    canActivate: [ PublicGuard ],
    loadChildren: './pages/common/signup/signup.module#SignupModule'
  },
  {
    path: '',
    canActivate: [ ProtectedGuard ],
    pathMatch: 'prefix',
    loadChildren: './pages/page.module#PageModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes, { enableTracing: false } // <-- for dev
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
