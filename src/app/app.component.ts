import { Component, ViewEncapsulation, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  title = 'app';

  ngOnInit(){

  }

}
