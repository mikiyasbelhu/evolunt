import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeModule } from './volunteer/home/home.module';
import { PageComponent } from './page.component';
import { TokenStorage } from '../services/auth/token-storage.service';
import { PageNotFoundComponent } from './common/errors/page-not-found.component';
import { RouteGuard } from '../services/auth/route-guard.service';
import { Component } from '@angular/core/src/metadata/directives';

export let authService: TokenStorage;

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    pathMatch: 'prefix',
    children: [
                {
                  path: '',
                  canActivate : [RouteGuard],
                  component : PageComponent 
                },
                {
                  path: 'v',
                  loadChildren: './volunteer/volunteer.module#VolunteerModule',
                },
                {
                  path: 'c',
                  loadChildren: './charity/charity.module#CharityModule',
                },
                {
                  path: 'setting',
                  loadChildren: './common/setting/setting.module#SettingModule',
                },
                {
                  path: 'profile',
                  loadChildren: './common/profile/profile.module#ProfileModule',
                },
                {
                  path: '**',
                  component: PageNotFoundComponent
                }
              ]
  }];
  @NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [
      RouterModule
    ],
    providers: [TokenStorage]
  })
export class PageRoutingModule { }
