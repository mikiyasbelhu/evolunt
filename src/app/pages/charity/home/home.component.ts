import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { PostsService } from '../../../services/posts/posts.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { NgForm } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css'],
  providers:[PostsService,ProfileService],
  // styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  public model;
  public currentUser;
  public role;
  public posts;
  public comments;
  public image:File;
  public posted = false;
  public posting = 'false';
  public loading = true;
  public length;
  public toLike;

  constructor(
    private router: Router, private authService: AuthenticationService,
    private _postsService:PostsService, private profileService:ProfileService){}
  
  ngOnInit(): void { 

    $('.ui.sticky')
    .sticky({

      pushing: false,
      offset : 80,
    });

    this.role = (this.authService.getRole());
    this.role = (this.role).charAt(0);
    this.router.navigateByUrl('/'+this.role+'/home');
    this.currentUser = this.profileService.getId();

    this.model = {
      id: this.profileService.getId(),
      post_message: ''
    };
    this.getFeed();
  }

  post(){
    this._postsService.charityPost(this.profileService.getId(),this.model).subscribe(data => {

      this.posted = true;
      this.getFeed();
      this.model.post_message = null;
    });
  }

  like(id){

    let data = {
      id: this.profileService.getId()
    }

    this._postsService.like(id, data).subscribe(data => {
      this.getFeed();
    });
  }

  unlike(id){
    let data = {
      id: this.profileService.getId()
    }

    this._postsService.unlike(id, data).subscribe(data => {
      this.getFeed();
    });
  }  

  comment(form: NgForm,id){

    let data = {
      volunteer: this.profileService.getId(),
        comment: form.value.data
    }

    this._postsService.comment(id, data).subscribe(data => {
      this.getFeed();
    });
  }

        /* property of File type */
        fileChange(event){

          this.image = event.target.files;
      }
 

  getFeed(){
    this._postsService.getCharityFeed(this.model.id).subscribe(data => {
      this.loading = false;
      
      this.posts = data.json().docs;  
      this.length = this.posts.length;    

    },
  error => {
    if(error.status == 403){
        this.authService.logout();
        this.router.navigateByUrl('/');
    }
    else{
      // this.getFeed();
    }
  });
  }

}
