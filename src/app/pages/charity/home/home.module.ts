import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { TimeAgoModuleModule } from '../../../directives/time-ago-module/time-ago-module.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    TimeAgoModuleModule
  ],
  declarations: [
    HomeComponent
  ],
  providers: []
})
export class HomeModule {
 

}
