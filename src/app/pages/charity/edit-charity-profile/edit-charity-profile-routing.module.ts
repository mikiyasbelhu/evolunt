import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditCharityProfileComponent } from './edit-charity-profile.component';




const usersRoutes: Routes = [
  { path: '',  component: EditCharityProfileComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EditCharityProfileRoutingModule { }
