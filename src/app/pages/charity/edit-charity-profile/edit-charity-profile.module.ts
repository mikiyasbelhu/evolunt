import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditCharityProfileComponent } from './edit-charity-profile.component';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';

import { FormsModule } from '@angular/forms'

import { HttpModule } from '@angular/http';
import { EditCharityProfileRoutingModule } from './edit-charity-profile-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    EditCharityProfileRoutingModule,
    TimeAgoModuleModule
  ], 
  declarations: [
    EditCharityProfileComponent
  ],
  providers: []
})
export class EditCharityProfileModule {
 

}
