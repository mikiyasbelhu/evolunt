import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { CharitiesService } from '../../../services/charities/charities.service';

@Component({
  selector: 'app-edit-charity-profile',
  templateUrl: './edit-charity-profile.component.html',
  styleUrls: ['./edit-charity-profile.component.css'],
  providers: [CharitiesService]
})
export class EditCharityProfileComponent implements OnInit {
  
  public name;
  public picture:FileList;
  public photo;
  public first;
  public uploading = 'false';
  public id;
  public user = {
    name:'',
    description:'',
    picture:null,
    date_created:'',
    followers:''
    // picture: null
  };

  constructor(public profileService: ProfileService,public charitiesService:CharitiesService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto();
    this.id = this.profileService.getId(); 
    this.first = true;

    this.charitiesService.get(this.id).subscribe(data => {

      this.user = data.json();

    });
  }

  submit(){
    this.first = false;
    
    this.charitiesService.update(this.id,this.user).subscribe(data => {

      this.user = data.json();
      
    });
  }

  back(){
    this.first = true;
  }

    /* property of File type */
    fileChange(event){
        // this.picture = event.target.files;
        this.picture = event.target.files;
    }

    /* Now send your form using FormData */
    upload(): void {

      this.uploading = 'true';   

      if(this.picture.length > 0) {
        let file: File = this.picture[0];
        let formData:FormData = new FormData();
        formData.append('picture', file, file.name);

        this.charitiesService.upload(this.id, formData).subscribe(
          data => {
            this.uploading = 'false';  
            this.user = data.json();
        },
        error => {
          this.uploading = 'false'; 
        }
        );
      }
    }   
  }
