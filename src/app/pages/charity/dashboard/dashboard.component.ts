import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  public name;
  public photo;

  constructor(public profileService: ProfileService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 
  }

}
