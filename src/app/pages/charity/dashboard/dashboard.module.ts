import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';

import { HttpModule } from '@angular/http';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    DashboardRoutingModule,
  ],
  declarations: [
    DashboardComponent
  ],
  providers: []
})
export class DashboardModule {
 

}
