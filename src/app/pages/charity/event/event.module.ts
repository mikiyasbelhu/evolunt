import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventComponent } from './event.component';

import { HttpModule } from '@angular/http';
import { EventRoutingModule } from './event-routing.module';
import { FormsModule } from '@angular/forms';

import { CreateEventComponent } from './create/create.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    EventRoutingModule,
  ],
  declarations: [
    EventComponent,
    CreateEventComponent
  ],
  providers: []
})
export class EventModule {
 

}
