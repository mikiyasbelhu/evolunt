import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { EventsService } from '../../../services/events/events.service';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  providers: [EventsService]
})
export class EventComponent implements OnInit {
  
  public name;
  public photo;
  public opportunities;
  public loading = true;
  public length;


  constructor(private router: Router,public profileService: ProfileService,
    public eventsService:EventsService, private authService: AuthenticationService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto();
    this.getOpportunities()
  }

  getOpportunities(){
    this.eventsService.getAll().subscribe(data => {

      this.loading = false;
      this.opportunities = data.json();
      this.opportunities = this.opportunities['docs'];
      this.length = this.opportunities.length;  
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{

        // this.getOpportunities();
      }    
    });
  }

}
