import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../../services/auth/profile.service';
import { EventsService } from '../../../../services/events/events.service';

declare var $: any;

@Component({
  selector: 'app-create-event',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [EventsService]
})
export class CreateEventComponent implements OnInit {
  
  public name;
  public photo;
  public event;
  public posted = false;
  public picture:FileList;
  public uploading = 'false';

  constructor(public profileService: ProfileService,
    public eventsService:EventsService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 

    this.event = {
      charity: this.profileService.getId(),
      title: '',
      description:'',
      location:'',
      date_of_event:''
    };

    $('.ui.dropdown')
    .dropdown();

  }

  submit(){

    $('.submit').addClass('loading');  
    // this.eventsService.create(this.event).subscribe(data => {
    //   $('.submit').removeClass('loading'); 
    //   this.posted = true;     
    //       },
    //     error => {
    //       $('.submit').removeClass('loading');
    //     });

        if(this.picture) {
          let file: File = this.picture[0];
          let formData:FormData = new FormData(this.event);


          formData.append('picture', file, file.name); 
          formData.append('charity',this.profileService.getId());    
          formData.append('title',this.event.title);    
          formData.append('description',this.event.description);    
          formData.append('location',this.event.location);  
          formData.append('date_of_event',this.event.date_of_event);    
        
          this.eventsService.create(formData).subscribe(data => {
    
            $('.submit').removeClass('loading'); 
            this.posted = true; 
        });
      }
      else{
        this.eventsService.create(this.event).subscribe(data => {
            $('.submit').removeClass('loading'); 
            this.posted = true;     
                },
              error => {
                $('.submit').removeClass('loading');
              });
      }



  }

   /* property of File type */
   fileChange(event){
    // this.picture = event.target.files;
    this.picture = event.target.files;
    
}   

}
