import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventComponent } from './event.component';
import { CreateEventComponent } from './create/create.component';

const usersRoutes: Routes = [
  { path: 'create',  component: CreateEventComponent },
  { path: '',  component: EventComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EventRoutingModule { }
