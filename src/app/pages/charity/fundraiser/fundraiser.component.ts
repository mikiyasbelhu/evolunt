import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { FundraiserService } from '../../../services/fundraiser/fundraiser.service';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-fundraiser',
  templateUrl: './fundraiser.component.html',
  styleUrls: ['./fundraiser.component.css'],
  providers: [FundraiserService, VolunteersService]
})
export class FundraiserComponent implements OnInit {
  
  public name;
  public photo;
  public campaigns;
  public percent = 59;
  public amount;
  public fundId;
  public loading = true;
  public loaded = false;

  constructor(private router: Router,public profileService: ProfileService,private volunteersService: VolunteersService,
    public fundraiserService:FundraiserService, private authService: AuthenticationService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto();
    this.getOpportunities();
  }


  getOpportunities(){
    this.fundraiserService.getAll().subscribe(data => {

      this.loading = false;
      this.campaigns = data.json();
      this.campaigns = this.campaigns['docs'];

      setTimeout(() => {
        this.loaded = true;
        $('.fund_progress').progress({
      })
    }, 100);
  

    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{

        // this.getOpportunities();
      }    
    });
  }

  public support(){

    let data = {
      fundID: this.fundId,
      amount_given: this.amount
    }

    this.volunteersService.contribute(this.profileService.getId(), data).subscribe(data => {
      this.getOpportunities();
    });
  }

  public contribute(id) {

    var self = this;
    this.fundId = id;
    $('.ui.mini.modal')
    .modal({
      onApprove : function() {
        self.support();
      }
    })  
    .modal('show');
  }

}
