import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../../services/auth/profile.service';
import { FundraiserService } from '../../../../services/fundraiser/fundraiser.service';

declare var $: any;

@Component({
  selector: 'app-create-fundraiser',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [FundraiserService]
})
export class CreateFundraiserComponent implements OnInit {
  
  public name;
  public photo;
  public picture:FileList;
  public posted = false;
  public fundraiser;

  constructor(public profileService: ProfileService,
    public fundraiserService:FundraiserService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 

    this.fundraiser = {
      charity: this.profileService.getId(),
      title: '',
      description:'',
      deadline:''
    };

    $('.ui.dropdown')
    .dropdown();

  }

  submit(){

    $('.submit').addClass('loading');  
    // this.fundraisersService.create(this.fundraiser).subscribe(data => {
    //   $('.submit').removeClass('loading'); 
    //   this.posted = true;     
    //       },
    //     error => {
    //       $('.submit').removeClass('loading');
    //     });

        if(this.picture) {
          let file: File = this.picture[0];
          let formData:FormData = new FormData(this.fundraiser);


          formData.append('picture', file, file.name); 
          formData.append('charity',this.profileService.getId());    
          formData.append('title',this.fundraiser.title);    
          formData.append('description',this.fundraiser.description);    
          formData.append('amount',this.fundraiser.amount);  
          formData.append('deadline',this.fundraiser.deadline);    
        
          this.fundraiserService.create(formData).subscribe(data => {
    
            $('.submit').removeClass('loading'); 
            this.posted = true; 
        });
      }
      else{
        this.fundraiserService.create(this.fundraiser).subscribe(data => {
            $('.submit').removeClass('loading'); 
            this.posted = true;     
                },
              error => {
                $('.submit').removeClass('loading');
              });
      }



  }

     /* property of File type */
     fileChange(event){
      // this.picture = event.target.files;
      this.picture = event.target.files;
      
  }
  

}
