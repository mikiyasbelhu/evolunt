import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FundraiserComponent } from './fundraiser.component';

import { HttpModule } from '@angular/http';
import { FundraiserRoutingModule } from './fundraiser-routing.module';
import { FormsModule } from '@angular/forms';

import { CreateFundraiserComponent } from './create/create.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    FundraiserRoutingModule,
  ],
  declarations: [
    FundraiserComponent,
    CreateFundraiserComponent
  ],
  providers: []
})
export class FundraiserModule {
 

}
