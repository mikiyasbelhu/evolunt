import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FundraiserComponent } from './fundraiser.component';
import { CreateFundraiserComponent } from './create/create.component';

const usersRoutes: Routes = [
  { path: 'create',  component: CreateFundraiserComponent },
  { path: '',  component: FundraiserComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class FundraiserRoutingModule { }
