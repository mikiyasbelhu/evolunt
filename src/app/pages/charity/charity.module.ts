import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './../../layout/menu/charity/menu.component';

import { CharityComponent } from './charity.component';
import { CharityRoutingModule } from './charity-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CharityRoutingModule,
  ],
  providers: [
    // DataService
  ],
  declarations: [CharityComponent, MenuComponent]
})
export class CharityModule { }
