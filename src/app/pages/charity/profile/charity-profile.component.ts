import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from '../../../services/posts/posts.service';
import { CharitiesService } from '../../../services/charities/charities.service';

@Component({
  selector: 'app-charity-profile',
  templateUrl: './charity-profile.component.html',
  styleUrls: ['./charity-profile.component.css'],
  providers: [CharitiesService,PostsService,ProfileService]
})
export class CharityProfileComponent implements OnInit {
  
  public user = {
    _id:'',
    name:'',
    interests:'',
    followers:'',
    following:'',
    description: '',
    last_name:'',
    picture: null
  };
  public page = 'home';
  public communityPage = 'following';
  public id;
  public posts;
  public loading_feed = true;
  public loading_user = true;
  public length;  
  public toFollow;
  public currentUser;
  public community = {
    followers: {
        charity: [],
        volunteer: []
    },
    following: {
        charity: [],
        volunteer: []
    }
};
  public isFollowing;

  constructor(private authService: AuthenticationService,  private router: Router, public charitiesService:CharitiesService,
    private _postsService:PostsService, public profileService: ProfileService,  route: ActivatedRoute){
  }
  
  ngOnInit(): void {

    this.currentUser = this.profileService.getId();

    this.getUser();
    this.getFeed();
    this.getFriends();
  }

  getFeed(){
    this._postsService.getUserFeed(this.profileService.getId()).subscribe(data => {
      this.loading_feed = false;
    
      this.posts = data.json().docs; 
      this.length = this.posts.length;       

    },
  error => {

    if(error.status == 403){
        this.authService.logout();
        this.router.navigateByUrl('/');
    }
    else{
      this.getFeed();
    }
  });
  }

  getUser(){
    this.charitiesService.get(this.profileService.getId()).subscribe(data => {

      this.loading_user = false;

      this.user = data.json();      
      
    },
  error => {
    if(error.status == 403){
        this.authService.logout();
        this.router.navigateByUrl('/');
    }
    else{
      // this.getUser();
    }
  });
  }

  follow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.charitiesService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {
    });    
  }

  followCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.charitiesService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {
    });    
  }

  unfollow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.charitiesService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }

  unfollowCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.charitiesService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }

  getFriends(){
    this.charitiesService.following(this.profileService.getId()).subscribe(data => {
      
      this.community = data.json();
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{
        // this.getFriends();
      }    
    });
  }

  public setPage (page){
    this.page = page;
  }

  public setCommunityPage (page){
    this.communityPage = page;
  }
  

}
