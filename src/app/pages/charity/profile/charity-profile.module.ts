import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharityProfileComponent } from './charity-profile.component';
import { FormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { CharityProfileRoutingModule } from './charity-profile-routing.module';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    CharityProfileRoutingModule,
    TimeAgoModuleModule
  ],
  declarations: [
    CharityProfileComponent
  ],
  providers: []
})
export class CharityProfileModule {
 

}
