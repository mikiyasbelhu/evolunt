import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PackageComponent } from './package.component';
import { CreatePackageComponent } from './create/create.component';

const usersRoutes: Routes = [
  { path: 'create',  component: CreatePackageComponent },
  { path: '',  component: PackageComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PackageRoutingModule { }
