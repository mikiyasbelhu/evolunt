import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../../services/auth/profile.service';
import { EventsService } from '../../../../services/events/events.service';
import { FundraiserService } from '../../../../services/fundraiser/fundraiser.service';

declare var $: any;

@Component({
  selector: 'app-create-package',
  templateUrl: './create.component.html',
  providers: [EventsService,FundraiserService]
})
export class CreatePackageComponent implements OnInit {
  
  public name;
  public photo;
  private model = {
    subcity: '',
    woreda: '',
    phone: ''
  };

  public package;
  public fund;


  public share = true;
  public tab: Tab[] = [{
    status:'active',
    form:false 
  },{
    status:'disabled',
    form:true 
  },
  {
    status:'disabled',
    form:true 
  },
  {
    status:'disabled',
    form:true 
  }
  ];

  constructor(public profileService: ProfileService,
    public eventsService:EventsService,
    public fundraiserService:FundraiserService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 

    this.package = {
      charity: this.profileService.getId(),
      title: '',
      description:'',
      location:'',
      date_of_package:''
    };

    this.fund = {
      created_by: this.profileService.getId(),
      title: '',
      description:'',
      location:'',
      amount:''
    };

    $('.ui.dropdown')
    .dropdown();



    // $('#rangestart').calendar({
    //   type: 'date',
    //   endCalendar: $('#rangeend')
    // });
    // $('#rangeend').calendar({
    //   type: 'date',
    //   startCalendar: $('#rangestart')
    // });

  }

  submit(tab){
    this.tab[tab].form = true;
    this.tab[tab].status = 'completed';
    this.tab[tab+1].form = false;
    this.tab[tab+1].status = 'active';
    if(tab == 0){
      this.eventsService.create(this.package).subscribe(data => {
            this.model = data.json();
          });
    }
    if(tab == 1){
      this.fundraiserService.create(this.fund).subscribe(data => {
            this.model = data.json();
          });
    }
  }

  back(tab){
    this.tab[tab].form = true;
    this.tab[tab].status = 'disbaled';
    this.tab[tab-1].form = false;
    this.tab[tab-1].status = 'active';
  }

  finish(){
    this.tab[3].form = true;
    this.tab[3].status = 'completed';
    this.share = false;
  }

  goTo(tab){
    
    this.share = true;
    
    for(let i = 0;i<(this.tab).length;i++){
      if(i==tab){
        this.tab[i].form = false;
        this.tab[i].status = 'active';
      }
      else if(this.tab[i].status == 'active'){
        this.tab[i].status = '';
        this.tab[i].form = true;
      }
      else{
        this.tab[i].form = true;
      }
    }    
  }

}

interface Tab {
  form: boolean;
  status : string;
}