import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackageComponent } from './package.component';

import { HttpModule } from '@angular/http';
import { PackageRoutingModule } from './package-routing.module';
import { FormsModule } from '@angular/forms';

import { CreatePackageComponent } from './create/create.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    PackageRoutingModule,
  ],
  declarations: [
    PackageComponent,
    CreatePackageComponent
  ],
  providers: []
})
export class PackageModule {
 

}
