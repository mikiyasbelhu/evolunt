import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharityCommunityComponent } from './charity-community.component';
// import { MenuComponent } from '../../../layout/menu/volunteer/menu.component';

import { HttpModule } from '@angular/http';
import { CharityCommunityRoutingModule } from './charity-community-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    CharityCommunityRoutingModule
  ],
  exports: [CharityCommunityComponent],
  declarations: [
    CharityCommunityComponent
    // MenuComponent
  ],
  providers: []
})
export class CharityCommunityModule {
 

}
