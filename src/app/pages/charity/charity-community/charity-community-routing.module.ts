import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharityCommunityComponent } from './charity-community.component';




const usersRoutes: Routes = [
  { path: '',  component: CharityCommunityComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CharityCommunityRoutingModule { }
