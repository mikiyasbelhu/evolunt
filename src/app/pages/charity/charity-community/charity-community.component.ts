import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { CharitiesService } from '../../../services/charities/charities.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/authentication.service';

declare var $: any;

@Component({
  selector: 'app-community',
  templateUrl: './charity-community.component.html',
  styleUrls: ['./charity-community.component.css'],
  providers: [CharitiesService],
})
export class CharityCommunityComponent implements OnInit {
  
  public name;
  public photo;
  public page = 'following';
  public community = {
          followers: {
              charity: [],
              volunteer: []
          },
          following: {
              charity: [],
              volunteer: []
          }
      };
  public followers;
  public following;
  public id;
  public loading = true;
  public toFollow;

  constructor(    private router: Router, private authService: AuthenticationService,
     public profileService: ProfileService, public charitiesService:CharitiesService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 
    this.id = this.profileService.getId()
    this.getFriends();
  }

  follow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.charitiesService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {

    });    
  }

  followCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.charitiesService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {

    });    
  }

  unfollow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.charitiesService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }


  unfollowCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }
  }

  

  getFriends(){
    this.charitiesService.following(this.id).subscribe(data => {

      this.loading = false;
      this.community = data.json();
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{
        // this.getFriends();
      }    
    });
  }

  public setPage (page){
    this.page = page;
  }
  

}
