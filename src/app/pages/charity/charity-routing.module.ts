import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharityComponent } from './charity.component';

const routes: Routes = [
  {
    path: '',
    component: CharityComponent,
    pathMatch: 'prefix',
    children: [
                {
                  path: 'dashboard',
                  loadChildren: './dashboard/dashboard.module#DashboardModule',
                },
                {
                  path: 'profile',
                  loadChildren: './profile/charity-profile.module#CharityProfileModule',
                },
                {
                  path: 'editprofile',
                  loadChildren: './edit-charity-profile/edit-charity-profile.module#EditCharityProfileModule',
                },
                {
                  path: 'community',
                  loadChildren: './charity-community/charity-community.module#CharityCommunityModule',
                },
                {
                  path: 'home',
                  loadChildren: './home/home.module#HomeModule',
                },
                {
                  path: 'event',
                  loadChildren: './event/event.module#EventModule',
                },
                {
                  path: 'fundraiser',
                  loadChildren: './fundraiser/fundraiser.module#FundraiserModule',
                },
                {
                  path: 'package',
                  loadChildren: './package/package.module#PackageModule',
                }
              ]
  }];
  @NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [
      RouterModule
    ]
  })
export class CharityRoutingModule { }
