import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  
  public name;
  public photo;

  constructor(public profileService: ProfileService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 
  }

}
