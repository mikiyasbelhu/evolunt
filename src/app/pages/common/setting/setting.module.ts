import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingComponent } from './setting.component';

import { HttpModule } from '@angular/http';
import { SettingRoutingModule } from './setting-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    SettingRoutingModule,
  ],
  declarations: [
    SettingComponent
  ],
  providers: []
})
export class SettingModule {
 

}
