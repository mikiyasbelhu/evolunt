import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/index';
 
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  public data;
  public error = {
    val:false,
    message:''}; 

  public model = {
    username: '',
    password: ''
  };
  
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }


  ngOnInit(): void {

    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form({
        fields: {
          username: {
            identifier  : 'username',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your e-mail'
              },
              {
                type   : 'email',
                prompt : 'Please enter a valid e-mail'
              }
            ]
          },
          password: {
            identifier  : 'password',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your password'
              },
              {
                type   : 'length[6]',
                prompt : 'Your password must be at least 6 characters'
              }
            ]
          }
        },
        inline: true,
        on: 'blur',
        // onSuccess: this.login()
           
        });
        $('.ui.form').submit(function(e){ 
          //e.preventDefault(); usually use this, but below works best here.
          return false;
      });
      
  }

  public login() {
    if( $('.ui.form').form('is valid')) {
      // form is valid (both email and name)
      $('.login').addClass('loading');
      this.authService
      .login(this.model)
      .subscribe(
        data => {
          this.authService.logout();
          this.data = data.json();
          this.authService.saveAccessData(this.data.token);
          // this.authService.saveAccessData('ehwkjb4ibjb3kj4b4b3iusnd');
          this.authService.saveProfile(this.data.user);
          this.authService.saveRole(this.data.user['user']['role']);

          if(this.data.user['user']['role']=="volunteer")
          {
            this.router.navigateByUrl('/v/home');
          }
          else {
            this.router.navigateByUrl('/c/home');
          }
        },
      error => {    

        if(error.status == 400){
            this.error.message = 'Incorrect username or password';    
          }
          else {
            this.error.message = 'Unable to connect please try again';
          }

          $('.login').removeClass('loading');
          this.error.val = true;

      });
  }else{

    }
  }

  signInWithGoogle(): void {
    console.log('Social auth not yet implemented');
  }

  signInWithFB(): void {
    console.log('Social auth not yet implemented');
  }

  signOut(): void {
  }


}
