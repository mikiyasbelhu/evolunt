import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/index';

declare var $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit{
  
    ngOnInit(): void {

    $('.ui.modal')
    .modal();
    
    $('.ui.checkbox').checkbox();
    $('.ui.form')
      .form({
        fields: {
          name: {
            identifier  : 'name',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter the name of your Charity'
              }
            ]
          },
          first_name: {
            identifier  : 'first_name',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your first name'
              }
            ]
          },
          last_name: {
            identifier  : 'last_name',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your last name '
              }
            ]
          },
          email: {
            identifier  : 'email',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your e-mail'
              },
              {
                type   : 'email',
                prompt : 'Please enter a valid e-mail'
              }
            ]
          },
          password: {
            identifier  : 'password',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your password'
              },
              {
                type   : 'length[6]',
                prompt : 'Your password must be at least 6 characters'
              }
            ]
          },
          terms: {
            identifier: 'terms',
            rules: [
              {
                type   : 'checked',
                prompt : 'You must agree to the terms and conditions'
              }
            ]
          }
        },
        inline: true,
        on: 'blur',
        // onSuccess: this.signup()
           
        });
        $('.ui.form').submit(function(e){ 
          //e.preventDefault(); usually use this, but below works best here.
          return false;
      });

      $('.ui.dropdown')
      .dropdown({
        action: 'activate'
      });
      
  }

  public model = {
    first_name: '',
    last_name: '',
    name: '',
    email: '',
    password: '',
    role: ''
  };

  public error = {
    val:false,
    message:''}; 

  public data;
  firstpage = true;
  volunteer = true;
  charity = true;

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  public signupVolunteer() {
    
    if( $('.ui.volunteer.form').form('is valid')) {
      $('.signupVolunteer').addClass('loading');
      
      this.authService
      .signup(this.model)
      .subscribe(
        data => {          
          this.data = data.json();                   
          this.router.navigateByUrl('/')},
          error =>{
              this.error.val = true;
              let temp = error.json();
              if(error.status == '0'){
                this.error.message = 'Unable to connect please try again';                    
              }
              else{
                this.error.message = temp.message;
              }

              $('.signupVolunteer').removeClass('loading');
          }
    );
      }else{
        this.error.val = true;
        $('.signupVolunteer').removeClass('loading');
    
  }

    }
    public signupCharity() {
      if( $('.ui.charity.form').form('is valid')) {
      
        $('.signupCharity').addClass('loading');
        this.authService
        .signup(this.model)
        .subscribe(
          data => {          
            this.data = data.json();                  
            this.router.navigateByUrl('/')},
            error =>{
              this.error.val = true;
              let temp = error.json();
              if(error.status == '0'){
                this.error.message = 'Unable to connect please try again';                    
              }
              else{
                this.error.message = temp.message;
              }

    
              $('.signupCharity').removeClass('loading');
            }
      );
        }else{
          this.error.val = true;
          $('.signupCharity').removeClass('loading');
      
    }
  
      }    

    public asCharity(){
      this.firstpage = false;
      this.charity = false;
      this.model.role = "charity";
    }

    public asVolunteer(){
      this.firstpage = false;
      this.volunteer = false;
      this.model.role = "volunteer";
    }

}
