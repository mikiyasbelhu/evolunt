import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';

import { FormsModule } from '@angular/forms'

import { HttpModule } from '@angular/http';
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ProfileRoutingModule,
  ], 
  declarations: [
    ProfileComponent
  ],
  providers: []
})
export class ProfileModule {
 

}
