import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [VolunteersService]
})
export class ProfileComponent implements OnInit {
  
  public name;
  public picture:File;
  public photo;
  public first;
  public id;
  public user = {
    first_name:'',
    last_name:'',
    description:'',
    picture: null
  };

  constructor(public profileService: ProfileService,public volunteerService:VolunteersService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto();
    this.id = this.profileService.getId(); 
    this.first = true;

    this.volunteerService.get(this.id).subscribe(data => {

      this.user = data.json();

    });
  }

  submit(){
    this.first = false;
    
    this.volunteerService.update(this.id,this.user).subscribe(data => {

      this.user = data.json();
      
    });
  }

  back(){
    this.first = true;
  }

    /* property of File type */
    fileChange(files: any){

        this.picture = files[0].nativeElement;
    }

    /* Now send your form using FormData */
    upload(): void {
    let _formData = new FormData();
    _formData.append("picture", this.picture);
    let body = _formData;

    this.volunteerService.upload(this.id,body).subscribe(data => {

      this.user = data.json();

    });
    }


}
