import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageComponent } from './message.component';

import { HttpModule } from '@angular/http';
import { MessageRoutingModule } from './message-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    MessageRoutingModule,
  ],
  declarations: [
    MessageComponent
  ],
  providers: []
})
export class MessageModule {
 

}
