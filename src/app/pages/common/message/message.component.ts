import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  
  public name;
  public photo;

  constructor(public profileService: ProfileService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 
  }

}
