import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../services/auth/index';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './page.component.html'
})
export class PageComponent implements OnInit{
  
  ngOnInit(): void {

    $('.ui.sticky')
    .sticky({

      pushing: false,
      offset : 80,
    });

    $('.card .dimmer')
    .dimmer({
      on: 'hover'
    });
  }

  public users$: Observable<any>;
  public data$: Observable<any>;

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  public loadData() {

  }

  public logout() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }

}
