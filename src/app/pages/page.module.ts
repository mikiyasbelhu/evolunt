import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { FooterComponent } from '../layout/footer.component';
import { NavComponent } from '../layout/nav/nav.component';
import { PageNotFoundComponent } from './common/errors/page-not-found.component';

@NgModule({
  imports: [
    CommonModule,
    PageRoutingModule,
  ],
  providers: [
    // DataService
  ],
  declarations: [PageComponent, FooterComponent, NavComponent,PageNotFoundComponent]
})
export class PageModule { }
