import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditVolunteerProfileComponent } from './edit-volunteer-profile.component';

const usersRoutes: Routes = [
  { path: '',  component: EditVolunteerProfileComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EditVolunteerProfileRoutingModule { }
