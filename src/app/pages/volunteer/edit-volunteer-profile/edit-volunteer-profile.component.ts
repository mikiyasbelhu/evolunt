import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';

declare var $: any;

@Component({
  selector: 'app-edit-volunter-profile',
  templateUrl: './edit-volunteer-profile.component.html',
  styleUrls: ['./edit-volunteer-profile.component.css'],
  providers: [VolunteersService]
})
export class EditVolunteerProfileComponent implements OnInit {
  
  public name;
  public picture:FileList;
  public photo;
  public first;
  public uploading = 'false';
  public id;
  public user = {
    first_name:'',
    last_name:'',
    picture:null,
    date_created:'',
    followers:'',
    description:'',
    interests:null
  };

  constructor(public profileService: ProfileService,public volunteersService:VolunteersService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto();
    this.id = this.profileService.getId(); 
    this.first = true;

    this.volunteersService.get(this.id).subscribe(data => {

      this.user = data.json();

      $('select.dropdown')
      .dropdown('set selected',(this.user['interests']));

    });
  }

  submit(){
    this.first = false;

    this.user.interests = $('#interests').dropdown('get value');
    
    this.volunteersService.update(this.id,this.user).subscribe(data => {

      this.user = data.json();
      
    },
  error => {
  });
  }

  back(){
    this.first = true;
  }

    /* property of File type */
    fileChange(event){
        // this.picture = event.target.files;
        this.picture = event.target.files;
    }

    /* Now send your form using FormData */
    upload(): void {

      this.uploading = 'true';   

      if(this.picture.length > 0) {
        let file: File = this.picture[0];
        let formData:FormData = new FormData();
        formData.append('picture', file, file.name);

        this.volunteersService.upload(this.id, formData).subscribe(
          data => {
            this.uploading = 'false';  
            this.user = data.json();
        },
        error => {
          this.uploading = 'false'; 
        }
        );
      }
    }   
  }
