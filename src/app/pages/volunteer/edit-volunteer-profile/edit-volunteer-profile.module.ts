import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditVolunteerProfileComponent } from './edit-volunteer-profile.component';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';

import { FormsModule } from '@angular/forms'

import { HttpModule } from '@angular/http';
import { EditVolunteerProfileRoutingModule } from './edit-volunteer-profile-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    EditVolunteerProfileRoutingModule,
    TimeAgoModuleModule
  ], 
  declarations: [
    EditVolunteerProfileComponent
  ],
  providers: []
})
export class EditVolunteerProfileModule {
 

}
