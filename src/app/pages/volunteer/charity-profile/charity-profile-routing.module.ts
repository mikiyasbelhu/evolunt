import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharityProfileComponent } from './charity-profile.component';




const usersRoutes: Routes = [
  { path: ':id/home',  component: CharityProfileComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CharityProfileRoutingModule { }
