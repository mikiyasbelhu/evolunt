import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharityProfileComponent } from './charity-profile.component';

import { HttpModule } from '@angular/http';
import { CharityProfileRoutingModule } from './charity-profile-routing.module';
import { FormsModule } from '@angular/forms';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';
import { FriendsModule } from '../friends/friends.module';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    CharityProfileRoutingModule,
    FormsModule,
    TimeAgoModuleModule,
    FriendsModule
  ],
  declarations: [
    CharityProfileComponent
  ],
  providers: []
})
export class CharityProfileModule {
 

}
