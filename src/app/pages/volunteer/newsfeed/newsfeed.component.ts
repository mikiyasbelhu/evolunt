import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { PostsService } from '../../../services/posts/posts.service';
import { log } from 'util';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { NgForm } from '@angular/forms';
import { TimeAgoPipe } from 'time-ago-pipe';

declare var $: any;

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  providers:[PostsService,ProfileService],
  styleUrls: ['./newsfeed.component.css']
})
export class NewsfeedComponent implements OnInit {
  
  public model;
  public currentUser;
  public image;
  public posts;
  public comments;
  public posted = false;
  public posting = 'false';
  public loading = true;
  public length;
  public toLike;

  constructor(
    private router: Router, private authService: AuthenticationService,
    private _postsService:PostsService, private profileService:ProfileService){}
  
  ngOnInit(): void { 

    this.currentUser = this.profileService.getId();

    $('.ui.sticky')
    .sticky({

      pushing: false,
      offset : 80,
    });

    this.model = {
      id: this.profileService.getId(),
      post_message: ''
    };
    this.getFeed();
  }

  post(){
    
    this.posting = 'true';
    if(this.image) {
      let file: File = this.image[0];
      let formData:FormData = new FormData(this.model);
      formData.append('picture', file, file.name); 
      formData.append('post_message',this.model.post_message);    
    
      this._postsService.volunteerPost(this.profileService.getId(),formData).subscribe(data => {

      this.posted = true;
      this.posting = 'false';
      this.getFeed();
      this.model.post_message = null;
    });
  }
  else{
    this._postsService.volunteerPost(this.profileService.getId(),this.model).subscribe(data => {

      this.posted = true;
      this.posting = 'false';
      this.getFeed();
      this.model.post_message = null;
    });
  }
  }

  like(id){

    let data = {
      id: this.profileService.getId()
    }

    this._postsService.like(id, data).subscribe(data => {
      this.getFeed();
    });
  }

  unlike(id){
    let data = {
      id: this.profileService.getId()
    }

    this._postsService.unlike(id, data).subscribe(data => {
      this.getFeed();
    });
  }  

  comment(form: NgForm,id){

    let data = {
      volunteer: this.profileService.getId(),
        comment: form.value.data
    }

    this._postsService.comment(id, data).subscribe(data => {
      this.getFeed();
    });
  }

 

  getFeed(){
    this._postsService.getFeed(this.model.id).subscribe(data => {
      this.loading = false;
      
      this.posts = data.json().docs;  
      this.length = this.posts.length;  

    },
  error => {
    if(error.status == 403){
        this.authService.logout();
        this.router.navigateByUrl('/');
    }
    else{
      // this.getFeed();
    }
  });
  }

      /* property of File type */
    fileChange(event){

      this.image = event.target.files;
  }

}
