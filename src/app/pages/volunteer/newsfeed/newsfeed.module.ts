import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsfeedComponent } from './newsfeed.component';
import { NewsfeedRoutingModule } from './newsfeed-routing.module';
import { FormsModule } from '@angular/forms';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';

@NgModule({
  imports: [
    CommonModule,
    NewsfeedRoutingModule,
    FormsModule,
    TimeAgoModuleModule
  ],
  declarations: [
    NewsfeedComponent,
  ],
  providers: []
})
export class NewsfeedModule {
 
 
}
