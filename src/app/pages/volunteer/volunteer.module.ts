import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './../../layout/menu/volunteer/menu.component';

import { VolunteerComponent } from './volunteer.component';
import { VolunteerRoutingModule } from './volunteer-routing.module';

@NgModule({
  imports: [
    CommonModule,
    VolunteerRoutingModule,
  ],
  providers: [
    // DataService
  ],
  declarations: [
    VolunteerComponent, 
    MenuComponent]
})
export class VolunteerModule { }
