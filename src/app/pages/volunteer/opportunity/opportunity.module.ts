import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OpportunityComponent } from './opportunity.component';
// import { MenuComponent } from '../../../layout/menu/volunteer/menu.component';

import { HttpModule } from '@angular/http';
import { OpportunityRoutingModule } from './opportunity-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    OpportunityRoutingModule,
  ],
  declarations: [
    OpportunityComponent,
    // MenuComponent
  ],
  providers: []
})
export class OpportunityModule {
 

}
