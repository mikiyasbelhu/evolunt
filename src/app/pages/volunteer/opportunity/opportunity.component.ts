import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { EventsService } from '../../../services/events/events.service';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { Router } from '@angular/router';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';

@Component({
  selector: 'app-opportunity',
  templateUrl: './opportunity.component.html',
  styleUrls: ['./opportunity.component.css'],
  providers: [EventsService, VolunteersService],
})
export class OpportunityComponent implements OnInit {
  
  public name;
  public photo;
  public opportunities;
  public loading = true;
  public length;
  public currentUser;

  constructor(
    private router: Router, private authService: AuthenticationService,  public volunteerService:VolunteersService,
    public profileService: ProfileService, public eventsService:EventsService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 
    this.currentUser = this.profileService.getId();

    this.getOpportunities();
  }

  getOpportunities(){
    this.eventsService.getAll().subscribe(data => {


      this.loading = false;
      this.opportunities = data.json();
      this.opportunities = this.opportunities['docs'];
      this.length = this.opportunities.length;  
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{
        // this.getOpportunities();
      }    
    });
  }

  attend(id){
    let data = {
      eventID: id
    }

    this.volunteerService.attend(this.profileService.getId(), data).subscribe(data => {
      this.getOpportunities();
    });
  }
  

}
