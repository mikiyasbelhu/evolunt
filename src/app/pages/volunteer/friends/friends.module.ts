import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FriendsComponent } from './friends.component';
// import { MenuComponent } from '../../../layout/menu/volunteer/menu.component';

import { HttpModule } from '@angular/http';
import { FriendsRoutingModule } from './friends-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FriendsRoutingModule
  ],
  exports: [FriendsComponent],
  declarations: [
    FriendsComponent
    // MenuComponent
  ],
  providers: []
})
export class FriendsModule {
 

}
