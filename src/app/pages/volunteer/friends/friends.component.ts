import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/authentication.service';

declare var $: any;

@Component({
  selector: 'app-community',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css'],
  providers: [VolunteersService],
})
export class FriendsComponent implements OnInit {
  
  public name;
  public photo;
  public page = 'following';
  public community = {
          followers: {
              charity: [],
              volunteer: []
          },
          following: {
              charity: [],
              volunteer: []
          }
      };
  public followers;
  public following;
  public id;
  public loading = true;
  public toFollow;

  constructor(    private router: Router, private authService: AuthenticationService,
     public profileService: ProfileService, public volunteersService:VolunteersService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 
    this.id = this.profileService.getId()
    this.getFriends();
  }

  follow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.volunteersService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {
    });    
  }

  followCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.volunteersService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {
    });    
  }

  unfollow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.volunteersService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }

  unfollowCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.volunteersService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }

  getFriends(){
    this.volunteersService.following(this.id).subscribe(data => {

      this.loading = false;
      this.community = data.json();
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{
        // this.getFriends();
      }    
    });
  }

  public setPage (page){
    this.page = page;
  }
  

}
