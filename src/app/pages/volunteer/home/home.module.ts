import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
// import { MenuComponent } from '../../../layout/menu/volunteer/menu.component';

import { HttpModule } from '@angular/http';
import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    HomeRoutingModule,
  ],
  declarations: [
    HomeComponent,
    // MenuComponent
  ],
  providers: []
})
export class HomeModule {
 

}
