import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  public name;
  public photo;

  constructor(public profileService: ProfileService){}
  
  ngOnInit(): void {
    this.name = this.profileService.getName();
    this.photo = this.profileService.getPhoto(); 

  }

}
