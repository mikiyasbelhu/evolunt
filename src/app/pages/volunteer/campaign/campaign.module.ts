import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignComponent } from './campaign.component';

import { HttpModule } from '@angular/http';
import { CampaignRoutingModule } from './campaign-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    CampaignRoutingModule,
  ],
  declarations: [
    CampaignComponent
  ],
  providers: []
})
export class CampaignModule {
 

}
