import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/auth/profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { PostsService } from '../../../services/posts/posts.service';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';
import { TimeAgoPipe } from 'time-ago-pipe';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
  providers:[PostsService,ProfileService,VolunteersService]
})
export class TimelineComponent implements OnInit {
  
  public user = {
    _id:'',
    first_name:'',
    interests:'',
    followers:'',
    following:'',
    last_name:'',
    picture: null
  };
  public page = 'home';
  public communityPage = 'following';
  public id;
  public posts;
  public loading_feed = true;
  public loading_user = true;
  public length;  
  public toFollow;
  public currentUser;
  public community = {
    followers: {
        charity: [],
        volunteer: []
    },
    following: {
        charity: [],
        volunteer: []
    }
};
  public isFollowing = false;

  constructor(private authService: AuthenticationService,  private router: Router, public volunteersService:VolunteersService,
    private _postsService:PostsService, public profileService: ProfileService,  route: ActivatedRoute){
    this.id = route.snapshot.params.id;
  }
  
  ngOnInit(): void {

    this.currentUser = this.profileService.getId();

    this.getUser();
    this.getFeed();
    this.getFriends();
  }

  getFeed(){
    this._postsService.getUserFeed(this.id).subscribe(data => {
      this.loading_feed = false;
    
      this.posts = data.json().docs; 
      this.length = this.posts.length;       

    },
  error => {

    if(error.status == 403){
        this.authService.logout();
        this.router.navigateByUrl('/');
    }
    else{
      this.getFeed();
    }
  });
  }

  getUser(){
    this.volunteersService.get(this.id).subscribe(data => {

      this.loading_user = false;

      this.user = data.json();      
      
    },
  error => {
    if(error.status == 403){
        this.authService.logout();
        this.router.navigateByUrl('/');
    }
    else{
      // this.getUser();
    }
  });
  }

  follow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.volunteersService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {
    });    
  }

  followCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.volunteersService.follow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends(); 
    },
    error => {
    });    
  }

  unfollow(toFollow){

    this.toFollow = {
      idToFollowV: toFollow
    }

    this.volunteersService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }

  unfollowCharity(toFollow){

    this.toFollow = {
      idToFollowC: toFollow
    }

    this.volunteersService.unfollow(this.profileService.getId(),this.toFollow).subscribe(data => {
      this.getFriends();      
    },
    error => {

    });    
  }

  getFriends(){
    this.volunteersService.following(this.id).subscribe(data => {
      
      this.community = data.json();
      if (this.community.followers.volunteer.some(e => e._id == this.currentUser)) {
        this.isFollowing = true;
      }
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
      else{
        // this.getFriends();
      }    
    });
  }

  like(id){

    let data = {
      id: this.profileService.getId()
    }

    this._postsService.like(id, data).subscribe(data => {
      this.getFeed();
    });
  }

  unlike(id){
    let data = {
      id: this.profileService.getId()
    }

    this._postsService.unlike(id, data).subscribe(data => {
      this.getFeed();
    });
  }  

  comment(form: NgForm,id){

    let data = {
      volunteer: this.profileService.getId(),
        comment: form.value.data
    }

    this._postsService.comment(id, data).subscribe(data => {
      this.getFeed();
    });
  }

  public setPage (page){
    this.page = page;
  }

  public setCommunityPage (page){
    this.communityPage = page;
  }
  
}
