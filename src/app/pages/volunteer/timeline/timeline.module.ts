import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineComponent } from './timeline.component';

import { HttpModule } from '@angular/http';
import { TimelineRoutingModule } from './timeline-routing.module';
import { FormsModule } from '@angular/forms';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';
import { FriendsModule } from '../friends/friends.module';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    TimelineRoutingModule,
    FormsModule,
    TimeAgoModuleModule,
    FriendsModule
  ],
  declarations: [
    TimelineComponent
  ],
  providers: []
})
export class TimelineModule {
 

}
