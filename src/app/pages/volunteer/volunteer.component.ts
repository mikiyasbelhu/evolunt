import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Component({
  selector: 'app-volunteer',
  templateUrl: './volunteer.component.html',
  styleUrls: ['./volunteer.component.css']
})
export class VolunteerComponent implements OnInit{
  
  ngOnInit(): void {
    $('.ui.sticky')
    .sticky({

      pushing: false,
      offset : 80,
    });
  }

}
