import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolunteerComponent } from './volunteer.component';

const routes: Routes = [
  {
    path: '',
    component: VolunteerComponent,
    pathMatch: 'prefix',
    children: [
                {
                  path: 'home',
                  loadChildren: './newsfeed/newsfeed.module#NewsfeedModule',
                },
                {
                  path: 'message',
                  loadChildren: './chat/chat.module#ChatModule',
                },
                {
                  path: 'editprofile',
                  loadChildren: './edit-volunteer-profile/edit-volunteer-profile.module#EditVolunteerProfileModule',
                },
                {
                  path: 'opportunity',
                  loadChildren: './opportunity/opportunity.module#OpportunityModule',
                },
                {
                  path: 'friends',
                  loadChildren: './friends/friends.module#FriendsModule',
                },
                {
                  path: 'c',
                  loadChildren: './charity-profile/charity-profile.module#CharityProfileModule',
                },
                {
                  path: 'campaign',
                  loadChildren: './campaign/campaign.module#CampaignModule',
                },
                {
                  path: ':id/home',
                  loadChildren: './timeline/timeline.module#TimelineModule',
                },
              ]
  }];
  @NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [
      RouterModule
    ]
  })
export class VolunteerRoutingModule { }
