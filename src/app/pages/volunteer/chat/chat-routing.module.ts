import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatComponent } from './chat.component';




const usersRoutes: Routes = [
  { path: '',  component: ChatComponent },
  { path: ':id',  component: ChatComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(usersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ChatRoutingModule { }
