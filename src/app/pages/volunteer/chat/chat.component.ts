import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { ChatService } from './../../../services/chat/chat.service';
import { ProfileService } from '../../../services/auth/profile.service';
import * as io from "socket.io-client";
import { AuthenticationService } from '../../../services/auth/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VolunteersService } from '../../../services/volunteers/volunteers.service';
import { NgForm } from '@angular/forms';
import { TimeAgoPipe } from 'time-ago-pipe';
declare var $: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  providers: [ChatService,VolunteersService]
  
})
export class ChatComponent implements OnInit, AfterViewChecked {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  public chats = [];
  public histories : any;
  public currentUser;
  public otherUser;
  public total = 0;
  public personal = false;
  public otherProfile = {
    _id:'',
    picture:'',
    first_name: '',
    last_name:''
  };
  msgData = {
    to_volunteer : '',
    from_volunteer: '',
    message: ''
 };
  socket = io('http://178.62.247.200:5800', {
    extraHeaders: {
      Authorization: "Bearer " + this.authService.getAccessToken()
    }
  });

  constructor( private router: Router,private authService: AuthenticationService,
    private chatService: ChatService,  public volunteersService:VolunteersService,
      public profileService: ProfileService,private route: ActivatedRoute) {
        if(route.snapshot.params.id){
          this.otherUser = route.snapshot.params.id;
        } else {
          this.otherUser = this.profileService.getId();
        }

        this.msgData.from_volunteer = this.currentUser = this.profileService.getId();

        if(this.currentUser == this.otherUser){
          this.personal = true;
        }
      }

 

  ngOnInit() {

    var temp = this;
    this.socket.on('connect', function (data) {
        temp.socket.emit('storeClientInfo', { customId: temp.currentUser });
    });

    this.getUser();

    this.getConversation();

    this.getInbox();

    this.socket.on('chat message', function (data) { 
      
      if(data.from_volunteer._id === this.currentUser) {
        this.msgData .message = '';
      }
      this.chats.push(data);
      // temp.scrollToBottom();
     }.bind(this));

     this.scrollToBottom();

  }

  refresh(id){

    this.otherUser = id;

    this.router.navigateByUrl('/v/message/'+ id );

    this.getUser();

    this.getConversation();

    this.getInbox();  
  }  

  getConversation(){

    let data = {
      to_volunteer : this.otherUser,
      from_volunteer: this.currentUser,   
    }

    this.chatService.getConversation(data).subscribe(data => {

      this.chats = (data.json().docs).reverse(); 
      this.total =  data.json().total_docs;   
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
      }
      else{
        // this.getUser();
      }
    });

    // this.scrollToBottom();   
  }

  getInbox(){

    let data = {
      volunteer : this.currentUser,   
    }

    this.chatService.getInbox(data).subscribe(data => {

      console.log(data.json()); 
      this.histories = data.json();
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
      }
      else{
        // this.getUser();
      }
    });

    // this.scrollToBottom();   
  }

  getUser(){
    this.volunteersService.get(this.otherUser).subscribe(data => {


      this.otherProfile = data.json();      
      
    },
  error => {
    if(error.status == 403){
        this.authService.logout();
    }
    else{
      // this.getUser();
    }
  });
  }


  ngAfterViewChecked() {        
    this.scrollToBottom();        
} 

scrollToBottom(): void {
    try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
}

  sendMessage(form: NgForm) {


  this.msgData = {
    to_volunteer : this.otherUser,
    from_volunteer: this.currentUser,
    message: form.value.data
 };
  this.socket.emit('chat message', this.msgData);
  this.getConversation();
  form.resetForm();
  // this.scrollToBottom();
  //  

  }

}