import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatComponent } from './chat.component';
import { ChatRoutingModule } from './chat-routing.module';
import { FormsModule } from '@angular/forms';
import { TimeAgoModuleModule } from './../../../directives/time-ago-module/time-ago-module.module';

@NgModule({
  imports: [
    CommonModule,
    ChatRoutingModule,
    FormsModule,
    TimeAgoModuleModule
  ],
  declarations: [ChatComponent]
})
export class ChatModule { }
