import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";
import { TimeAgoPipe } from 'time-ago-pipe';

@Pipe({
    name: "timeAgo"
})
export class TimeAgoCustomPipe extends TimeAgoPipe {}