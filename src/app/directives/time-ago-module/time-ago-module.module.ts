import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeAgoCustomPipe } from './time-ago-pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TimeAgoCustomPipe],
  exports: [
    TimeAgoCustomPipe
  ]
})
export class TimeAgoModuleModule { 

}
